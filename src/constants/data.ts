import { TimelineItem } from "@/components/timeline-item/types";

const data: TimelineItem[] = [
  {
    date: "1951",
    title: "UNIVAC",
    subtitle: "The first commercial computer",
    content:
      "The UNIVAC (Universal Automatic Computer) was the first commercial computer produced in the United States. It was large, expensive, and primarily used by businesses. This milestone marked the beginning of the era of commercial computing technology.",
    image: "univac.jpg",
  },
  {
    date: "1960",
    title: "LARC",
    subtitle: "First super computer",
    content:
      "The Livermore Atomic Research Computer (LARC) is introduced in 1960, marking one of the first supercomputers. Developed by the Lawrence Livermore National Laboratory, LARC was used for complex scientific calculations and research.",
  },
  {
    date: "1963",
    title: "Cassette Tape",
    subtitle: "A new way to store music",
    content:
      "In 1963, Philips introduced the compact audio cassette tape, revolutionizing the way people stored and played music. The cassette tape quickly gained popularity as a convenient and portable music storage medium, influencing the music industry and personal audio consumption for decades to come.",
    image: "cassette-tape.jpg",
  },
  {
    date: "1964",
    title: "First Modern Fax Machine",
    subtitle: "Transmitting images electronically",
    content:
      "The first modern fax machine, developed by Xerox, is introduced. It revolutionizes communication by allowing images and documents to be transmitted electronically over telephone lines, marking a significant advancement in office technology.",
  },
  {
    date: "1964",
    title: "Computer Mouse",
    subtitle: "A revolutionary new kind of input device",
    content:
      "The first computer mouse, made partly out of wood, was invented at SRI by Douglas Engelbart. This innovation played a crucial role in shaping modern computer interaction and became an integral part of personal computing.",
    image: "mouse.jpg",
  },
  {
    date: "1964",
    title: "Motorola Pageboy",
    subtitle: "Early portable messaging device",
    content:
      "The Motorola Pageboy I, introduced in 1964, is recognized as one of the earliest pagers designed for sending messages to portable devices. It allowed users to receive short text messages via radio signals, laying the foundation for modern paging technology.",
  },
  {
    date: "1964",
    title: "BASIC Programming Language",
    subtitle: "A more accessible programming language",
    content:
      "Two Dartmouth professors, John Kemeny and Thomas Kurtz, create the BASIC (Beginner's All-Purpose Symbolic Instruction Code) language to allow students to write computer programs more easily. BASIC played a significant role in making programming more accessible to a wider audience, contributing to the growth of the computing industry.",
  },
  {
    date: "1967",
    title: "First ATM",
    subtitle: "Revolutionizing banking technology",
    content:
      "The first Automated Teller Machine (ATM) is installed by Barclays Bank in London. It allows customers to withdraw cash, check their balance, and perform other banking transactions without visiting a bank branch, revolutionizing the way people access their money.",
    image: "atm.jpg",
  },
  {
    date: "1969",
    title: "UNIX",
    subtitle: "The foundation of modern operating systems",
    content:
      "UNIX, developed at AT&T's Bell Labs, is a powerful and versatile operating system that becomes the foundation for many other operating systems, including Linux and macOS.",
  },
  {
    date: "1969",
    title: "ARPANET",
    subtitle: "Foundations of the Internet",
    content:
      "ARPANET, consisting of four nodes, comes online, eventually leading to the Internet. This groundbreaking development laid the foundation for the modern digital age and transformed the way people communicate and access information.",
  },
  {
    date: "1969",
    title: "CompuServe",
    subtitle: "The earliest online service",
    content:
      "CompuServe is the first major commercial online service provider in the United States, based in Ohio. It played a pivotal role in the early days of online communication and paved the way for the Internet services we use today.",
    image: "compuserve.jpg",
  },
  {
    date: "1971",
    title: "First Computer Virus",
    subtitle: "Emergence of computer malware",
    content:
      'In 1971, the Creeper virus becomes one of the first instances of computer malware. It was designed to infect early computer networks and displayed the message "I\'m the creeper, catch me if you can!".',
  },
  {
    date: "1972",
    title: "C Programming Language",
    subtitle: "A foundational procedural language",
    content:
      "C is a general-purpose programming language created by Dennis Ritchie at Bell Labs in 1972. It has been influential in the development of many other programming languages and is known for its simplicity and efficiency. C is widely used in system programming, embedded systems, and application development.",
  },
  {
    date: "1972",
    title: "Pong",
    subtitle: "The first hit computer game",
    content:
      "Pong was the first video game to stand alongside pinball machines and was introduced by Atari. This simple yet addictive game marked the birth of the video game industry and laid the groundwork for the gaming culture we know today.",
    image: "pong.gif",
  },
  {
    date: "1972",
    title: "Atari 2600",
    subtitle: "A popular home game console",
    content:
      "The Atari 2600 is released, becoming the first widely popular home video game console. It brought the arcade experience into people's living rooms and helped shape the gaming industry.",
    image: "atari-2600.jpg",
  },
  {
    date: "1976",
    title: '5.25" Floppy Disk',
    subtitle: "Revolution in data storage",
    content:
      "Shugart Associates introduced the 5.25-inch floppy disk, the first widespread portable computer storage disk. The capacity increased from 160kb to 360kb and eventually to 1.2mb by 1984. The disks were thin and could be easily damaged, but they played a crucial role in data storage during the early days of personal computing.",
    image: "5-inch-floppy.jpg",
  },
  {
    date: "1976",
    title: "VHS Tape",
    subtitle: "A new way to watch movies at home",
    content:
      "The VHS tape allowed people to watch movies at home, which was a big change from going to movie theaters. It revolutionized the home entertainment industry and transformed the way people consumed visual content.",
    image: "vhs-tape.jpg",
  },
  {
    date: "1977",
    title: "Apple II",
    subtitle: "First popular home computer",
    content:
      "The Apple II, made by Apple, was a computer aimed at everyday users, which becomes a tremendous success. It played a pivotal role in popularizing personal computing and laying the groundwork for future innovations from Apple.",
    image: "apple-ii.jpg",
  },
  {
    date: "1979",
    title: "Sony Walkman",
    subtitle: "Music becomes portable",
    content:
      "Sony introduces the Walkman, which allows people to listen to music on the go and becomes widely popular. It marked a significant shift in the way people enjoyed music and shaped the portable audio industry.",
    image: "sony-walkman.jpg",
  },
  {
    date: "1980s",
    title: "28k Modem",
    subtitle: "Early Internet connectivity technology",
    content:
      "The 28k modem, operating at a speed of 28,800 bits per second, represents an early stage in the evolution of Internet connectivity. It allows users to access online services and browse the web, albeit at relatively slow speeds.",
  },
  {
    date: "1980s",
    title: "Usenet",
    subtitle: "Discussion system and precursor to forums",
    content:
      "Usenet, a worldwide distributed discussion system, is established. It serves as an early Internet forum, allowing users to post and read messages on various topics. Usenet becomes a precursor to modern online forums and communities.",
  },
  {
    date: "1981",
    title: "MS-DOS",
    subtitle: "The first popular PC operating system",
    content:
      "Microsoft releases MS-DOS, which becomes a universal operating system for the IBM PC and PC clones throughout the 1980s. It was a command-prompt operating system with no graphical interface. MS-DOS played a pivotal role in the early days of personal computing.",
    image: "ms-dos.png",
  },
  {
    date: "1981",
    title: "IBM PC",
    subtitle: "A computer for home and work",
    content:
      "The IBM Personal Computer (PC), which inspired many generic clones, becoming popular both at home and work, is released. It standardized personal computing hardware and helped establish the PC as a household and business tool.",
    image: "ibm-pc.jpg",
  },
  {
    date: "1982",
    title: "Sony Watchman",
    subtitle: "Portable television",
    content:
      "Sony Watchman is a series of portable televisions introduced by Sony. It allows users to watch television broadcasts on a small, handheld device, making it a popular choice for on-the-go entertainment.",
    image: "sony-watchman.jpg",
  },
  {
    date: "1983",
    title: "C++ Programming Language",
    subtitle: "An extension of C with object-oriented features",
    content:
      "C++ is an extension of the C programming language with added support for object-oriented programming. It was developed by Bjarne Stroustrup at Bell Labs in 1983. C++ is used for a wide range of applications, including game development, system software, and high-performance applications.",
  },
  {
    date: "1983",
    title: "Digital Audio Compact Disc",
    subtitle: "A modern way to store high-quality music and audio",
    content:
      "The Digital Audio Compact Disc is introduced, which becomes the standard for music storage. In 1988, the CD-ROM was introduced as a storage medium for computers. Later on, re-writable disks (CD-R/CD-RW) were introduced. This technology revolutionized audio and data storage.",
    image: "audio-cd.jpg",
  },
  {
    date: "1983",
    title: "Control Video Corporation",
    subtitle: "The predecessor to AOL",
    content:
      "Control Video Corporation (CVC) is founded, which later becomes America Online (AOL), the most popular Internet provider in the 1990s. It played a crucial role in introducing millions of people to the Internet and online communication.",
  },
  {
    date: "1983",
    title: "Motorola DynaTAC",
    subtitle: "A large and expensive mobile phone",
    content:
      "The Motorola DynaTAC 8000X is released, which becomes a popular mobile phone for those who can afford its $4,000 price tag. After charging for 10 hours, it provided 30 minutes of talk time. It was an early milestone in the evolution of mobile communication technology.",
    image: "motorola-dynatac.jpg",
  },
  {
    date: "1985",
    title: "Microsoft Windows 1.0",
    subtitle: "The first graphical operating system for the PC",
    content:
      "Windows 1.0, Microsoft's answer to the Macintosh, is released. It marked Microsoft's entry into the graphical user interface era and set the stage for future Windows versions that would dominate the personal computer market.",
    image: "windows-1.jpg",
    link: "https://www.howtogeek.com/700661/35-years-of-microsoft-windows-remembering-windows-1.0",
  },
  {
    date: "1985",
    title: "CD-ROM Introduced",
    subtitle: "A big step for computer data storage",
    content:
      "The CD-ROM is introduced, which allows for much more data to be stored on a single disk. It became popular in the 1980s and 1990s for computer software, multimedia, and video games. CD-ROMs transformed how software and media were distributed.",
    image: "cd-rom.jpg",
  },
  {
    date: "1985",
    title: "Nintendo Entertainment System",
    subtitle: "A game console that becomes a world-wide hit",
    content:
      "Nintendo Entertainment System (NES) becomes widely popular with games such as Duck Hunt, Super Mario Bros, The Legend of Zelda, and Mike Tyson's Punchout. It revitalized the video game industry after the North American video game crash of 1983 and set the stage for Nintendo's future success.",
    image: "nintendo-nes.jpg",
  },
  {
    date: "1986",
    title: '3.5" Floppy Disk',
    subtitle: "The most popular floppy disk until CDs",
    content:
      "IBM started using a 720 KB double density 3.5-inch microfloppy disk in 1986 and the 1.44 MB high-density version in 1987. The 3.5-inch disk quickly caught on in popularity for PCs. The disks also had a write-protection tab, which was a notch in the top corner of the disk. They became a standard medium for data storage during the late 1980s and 1990s.",
    image: "3-inch-floppy.jpg",
  },
  {
    date: "1986",
    title: "Lazer Tag",
    subtitle: "A popular toy in the late 1980s",
    content:
      "Lazer Tag, also known as Laser Tag, is released, which becomes a popular pursuit game with infrared guns. It was a significant cultural phenomenon in the 1980s, reflecting the era's fascination with high-tech toys and experiences.",
    image: "lazer-tag.jpg",
  },
  {
    date: "1987",
    title: "Perl Programming Language",
    subtitle: "A language optimized for text processing",
    content:
      "Perl, created by Larry Wall, is a versatile and widely-used scripting language. It is known for its powerful text processing features and is commonly used for web development, system administration, and network programming.",
  },
  {
    date: "1988",
    title: "Sega Genesis",
    subtitle: "A popular video game console",
    content:
      "Sega Genesis, also known as Mega Drive, is released, which becomes a popular video game console. It also introduces the popular game, Sonic the Hedgehog. Sega Genesis played a crucial role in the console wars of the 1990s.",
    image: "sega-genesis.jpg",
  },
  {
    date: "1989",
    title: "World Wide Web Proposal",
    subtitle: "Birth of the World Wide Web",
    content:
      "Tim Berners-Lee proposes the World Wide Web to his employer, CERN. This proposal marked the beginning of the World Wide Web as we know it, leading to the development of the Internet as a global information and communication platform.",
    image: "berners-lee-www.jpg",
  },
  {
    date: "1989",
    title: "Motorola MicroTAC",
    subtitle: "A handheld, foldable mobile phone",
    content:
      "The Motorola MicroTAC is released, which becomes the first widely available mobile phone. Its compact and foldable design represented a significant step forward in mobile phone technology, making communication more accessible to the masses.",
    image: "motorola-microtac.jpg",
  },
  {
    date: "1989",
    title: "SimCity",
    subtitle: "A popular city-building game",
    content:
      "SimCity, a city-building simulation game developed by Will Wright, was released. It allowed players to design and manage their own virtual cities, and it became a beloved classic in the world of gaming.",
    image: "simcity.png",
  },
  {
    date: "1989",
    title: "Nintendo Game Boy",
    subtitle: "A portable game console that becomes a huge hit",
    content:
      "Nintendo Game Boy, a handheld gaming console, was introduced, becoming a massive success. It popularized portable gaming and set the stage for a long line of successful Nintendo handheld consoles.",
    image: "nintendo-gameboy.jpg",
  },
  {
    date: "1989",
    title: "Atari Lynx",
    subtitle: "The first handheld color game console",
    content:
      "Atari Lynx, the first handheld color game console, was released. While it faced competition from Nintendo's Game Boy, it marked a significant advancement in handheld gaming technology.",
    image: "atari-lynx.jpg",
  },
  {
    date: "1990s",
    title: "56k Modem",
    subtitle: "Improving Internet speeds for home users",
    content:
      "The 56k modem, operating at a speed of 56,000 bits per second, represents a significant improvement in home Internet connectivity. It enables faster downloads and smoother online experiences for users during the dial-up Internet era.",
  },
  {
    date: "1990",
    title: "Sound Blaster",
    subtitle: "A sound card that raised the bar for PC audio",
    content:
      "Sound Blaster, a series of sound cards by Creative Technology, raised the bar for PC audio quality. It played a crucial role in making personal computers capable of producing high-quality audio, contributing to the growth of multimedia and gaming on PCs.",
    image: "sound-blaster.jpg",
  },
  {
    date: "1990",
    title: "Windows 3.0",
    subtitle: "A very popular version of Windows",
    content:
      "A big improvement over earlier versions, Windows 3.0 and 3.1 (launched in 1992) became ubiquitous in the 1990s. These versions of Windows introduced significant graphical user interface improvements and helped Microsoft solidify its dominance in the personal computer market.",
    image: "windows-3.jpg",
  },
  {
    date: "1991",
    title: "Python Programming Language",
    subtitle: "A modern programming language with clean syntax",
    content:
      "Guido van Rossum releases the Python programming language, known for its clean syntax, which develops over time to become one of the most popular programming languages in the world. Python has become essential in various fields, from web development to scientific computing.",
  },
  {
    date: "1991",
    title: "MP3 Audio Format",
    subtitle: "The era of digital music",
    content:
      "The MP3 audio format, developed in Germany, is released, which becomes the standard for digital music. MP3s revolutionized how music was stored, shared, and consumed, shaping the digital music era.",
  },
  {
    date: "1991",
    title: "Linux",
    subtitle: "A free and open-source operating system",
    content:
      "Linux is first released by Linus Torvalds. It becomes a popular free and open-source operating system, powering servers, supercomputers, and embedded devices worldwide.",
    image: "linus-linux.jpg",
  },
  {
    date: "1991",
    title: "SSD",
    subtitle: "A faster and more reliable storage medium",
    content:
      "The solid-state drive (SSD) is introduced, which is faster and more reliable than hard disk drives (HDDs). SSDs have since become the standard for data storage, offering improved speed and durability.",
  },
  {
    date: "1992",
    title: "Wolfenstein 3D",
    subtitle: "The first popular first-person shooter",
    content:
      "Wolfenstein 3D is released, which set the standard for 3D point of view graphics video games. It was a pioneering title in the first-person shooter genre, laying the groundwork for games like Doom and Quake.",
    image: "wolfenstein-3d.jpg",
  },
  {
    date: "1992",
    title: "Sony MiniDisc",
    subtitle: "A hit in Asia but not in the West",
    content:
      "The Sony MiniDisc was a popular digital storage medium for audio, but it only achieved limited success in the US and Europe. It offered a compact and recordable format for audio, but it faced competition from other emerging technologies.",
    image: "sony-minidisc.jpg",
  },
  {
    date: "1993",
    title: "Doom",
    subtitle: "A popular first-person shooter with 3D graphics",
    content:
      "Doom, developed by id Software, was a landmark title in the first-person shooter genre. It gained popularity for its fast-paced action and impressive 3D graphics, setting the stage for the future of 3D gaming.",
    image: "doom.jpg",
  },
  {
    date: "1993",
    title: "Myst",
    subtitle: "A computer game with impressive graphics",
    content:
      "Myst, an adventure puzzle game, featured stunning graphics and was one of the first games to be released on CD-ROM. It became a best-selling title and showcased the potential of multimedia gaming.",
    image: "myst.jpg",
  },
  {
    date: "1993",
    title: "Microsoft Encarta",
    subtitle: "A popular digital encyclopedia on CD-ROM",
    content:
      "Microsoft Encarta, a digital encyclopedia, gained popularity as it provided a wealth of information in a multimedia format. It was widely used in schools and homes as a reference tool.",
    image: "microsoft-encarta.jpg",
  },
  {
    date: "1993",
    title: "Apple Newton",
    subtitle: "One of the first personal digital assistants",
    content:
      "Apple Newton, introduced by Apple Inc., is one of the earliest personal digital assistants (PDAs). Although it faced challenges, it laid the groundwork for future handheld devices and introduced concepts like handwriting recognition.",
    image: "newton.jpg",
  },
  {
    date: "1994",
    title: "Netscape Navigator",
    subtitle: "The first popular web browser",
    content:
      "Netscape Navigator was the first popular graphical browser for the World Wide Web. It played a pivotal role in making the Internet accessible to the general public and contributed to the dot-com boom of the late 1990s.",
    image: "netscape.jpg",
  },
  {
    date: "1994",
    title: "Lycos",
    subtitle: "Web search engine and web portal",
    content:
      "Lycos is introduced as one of the first search engines and web portals, offering users the ability to search for information, browse the web, and access various online services. It becomes one of the most visited websites during the early days of the Internet.",
    image: "lycos.png",
  },
  {
    date: "1994",
    title: "Zip Drive",
    subtitle: "A new high-capacity floppy disk",
    content:
      "Iomega released a high-capacity floppy drive known as the Zip drive. The original capacity of a Zip disk was 100 MB and eventually expanded to 750 MB. Zip drives provided a convenient and higher-capacity alternative to standard floppy disks.",
    image: "zip-drive.jpg",
  },
  {
    date: "1994",
    title: "Amazon.com",
    subtitle: "The beginning of the online shopping revolution",
    content:
      "Amazon is founded by Jeff Bezos in Seattle, Washington. Originally an online bookstore, Amazon eventually becomes the world's largest online retailer, revolutionizing the way people shop for goods.",
    image: "amazon.jpg",
  },
  {
    date: "1994",
    title: "Geocities Websites",
    subtitle: "Personal web hosting service",
    content:
      "Geocities is launched, allowing users to create their own websites and share content online. It becomes one of the first popular web hosting services, enabling people to express themselves and connect with others through personal webpages.",
    image: "geocities.png",
  },
  {
    date: "1994",
    title: "Bluetooth",
    subtitle: "Wireless technology standard",
    content:
      "Bluetooth technology is developed as a wireless communication standard for short-range connections between devices. It enables devices such as smartphones, laptops, and earphones to communicate and share data without the need for cables.",
  },
  {
    date: "1994",
    title: "Sony PlayStation",
    subtitle: "Sony's first video game console",
    content:
      "The Sony PlayStation, also known as PS one, is released, which becomes the first video game console to sell 100 million units. It marked Sony's entry into the gaming console market and established PlayStation as a dominant brand.",
    image: "sony-playstation.jpg",
  },
  {
    date: "1995",
    title: "Java Programming Language",
    subtitle: "A powerful object-oriented language",
    content:
      "Java is a versatile, class-based, and object-oriented programming language. It is known for its platform independence, thanks to the Java Virtual Machine (JVM). Java is widely used in web and mobile application development, as well as enterprise software.",
  },
  {
    date: "1995",
    title: "JavaScript Programming Language",
    subtitle: "The programming language of the web",
    content:
      "Brendan Eich creates JavaScript, originally called LiveScript, which allows web browsers to update and modify webpages, making the content more dynamic. JavaScript has since become essential for web development.",
  },
  {
    date: "1995",
    title: "Ruby Programming Language",
    subtitle: "A human-friendly programming language",
    content:
      "Ruby, designed by Yukihiro Matsumoto, is a dynamic and object-oriented programming language known for its simplicity and productivity. It gains popularity among developers for its elegant syntax and flexibility.",
  },
  {
    date: "1995",
    title: "eBay",
    subtitle: "Online auction and shopping website",
    content:
      "eBay is founded, creating a platform for online auctions and shopping. It enables individuals and businesses to buy and sell a wide variety of goods and services globally. eBay becomes one of the pioneers of e-commerce.",

    image: "ebay.png",
  },
  {
    date: "1995",
    title: "AltaVista",
    subtitle: "Early web search engine",
    content:
      "AltaVista is launched as one of the earliest web search engines, providing users with the ability to search for information on the World Wide Web. It quickly gains popularity as a powerful search tool before being overtaken by competitors.",
    image: "altavista.jpg",
  },
  {
    date: "1995",
    title: "Yahoo",
    subtitle: "Web portal and search engine",
    content:
      "Yahoo is a prominent web portal and search engine that provides a variety of services, including email, news, and online directories. It becomes one of the most visited websites on the Internet.",
    image: "yahoo.png",
  },
  {
    date: "1995",
    title: "RealPlayer",
    subtitle: "Media player and streaming software",
    content:
      "RealPlayer, one of the early multimedia players, is released. It allows users to play audio and video files on their computers and stream content from the Internet. RealPlayer contributes to the popularization of online multimedia consumption.",
  },
  {
    date: "1996",
    title: "ICQ",
    subtitle: "The pioneering instant messaging service",
    content:
      "ICQ, developed by Mirabilis, is one of the first instant messaging applications, allowing users to send real-time text messages and files over the Internet. It plays a significant role in the early days of online communication.",
    image: "icq.png",
  },
  {
    date: "1996",
    title: "Adobe Flash",
    subtitle: "Multimedia software platform",
    content:
      "Adobe Flash is introduced, revolutionizing online multimedia content. It enables the creation of interactive animations, games, and multimedia applications on the web. Flash content becomes widespread on websites and online applications.",
    image: "adobe-flash.jpg",
  },
  {
    date: "1996",
    title: "USB",
    subtitle: "The standard for computer device connections",
    content:
      "USB (Universal Serial Bus) is released, which becomes the standard for connecting devices to computers. It simplified and standardized the connection of various peripherals, such as keyboards, mice, and storage devices.",
  },
  {
    date: "1996",
    title: "DVD",
    subtitle: "High quality video on a disk",
    content:
      "The DVD (Digital Video Disk) is introduced, which allows for high quality video, up to 4.7 GB, to be stored on a disk. DVDs replaced VHS tapes as the standard for home video entertainment.",
  },
  {
    date: "1996",
    title: "Tamagotchi",
    subtitle: "A handheld digital pet",
    content:
      'Tamagotchi, in Japanese meaning "Egg watch," is released and becomes one of the biggest toy fads of the 1990s and early 2000s. It popularized virtual pets and digital companions.',
    image: "tamagotchi.jpg",
    link: "https://tamagotchi.bandai.com/",
  },
  {
    date: "1997",
    title: "Palm Pilot",
    subtitle: "A popular handheld organizer",
    content:
      "Palm Pilot, created by Palm, Inc., is a highly popular handheld organizer and personal information manager. It becomes widely adopted for its user-friendly interface and features like calendar, contacts, and note-taking.",
    image: "palm-pilot.png",
  },
  {
    date: "1997",
    title: "IBM Deep Blue Wins at Chess",
    subtitle: "AI triumphs against a human",
    content:
      "Deep Blue, a chess-playing computer developed by IBM, defeats the world chess champion Garry Kasparov in a highly publicized match. This event marked a significant milestone in artificial intelligence and computer gaming.",
    image: "ibm-deep-blue.jpg",
  },
  {
    date: "1998",
    title: "PayPal",
    subtitle: "Online payments and money transfers",
    content:
      "PayPal is established as a secure way to make online payments and money transfers. It provides a convenient and safe method for individuals and businesses to conduct financial transactions over the Internet, leading to the growth of online commerce.",
    image: "paypal.png",
  },
  {
    date: "1998",
    title: "Apple iMac",
    subtitle: "A popular all-in-one computer",
    content:
      "The iMac was a popular all-in-one computer that came in a variety of colors. It became widely popular and began the resurgence of Apple as a major player in the tech industry.",
    image: "imac.jpg",
  },
  {
    date: "1999",
    title: "Blackberry",
    subtitle: "The iconic mobile device for professionals",
    content:
      "Blackberry phones, developed by Research In Motion (RIM), are known for their physical keyboards and secure email services. They gain popularity among professionals and business users for their messaging capabilities.",
    image: "blackberry.png",
  },
  {
    date: "1999",
    title: "Napster",
    subtitle: "Pioneering music file sharing service",
    content:
      "Napster, created by Shawn Fanning, is a pioneering music file sharing service that introduces the concept of peer-to-peer sharing of MP3 files. It becomes hugely popular but faces legal challenges from the music industry.",
    image: "napster.jpg",
  },
  {
    date: "1999",
    title: "Y2K",
    subtitle: "Year 2000 problem",
    content:
      "Y2K, also known as the Millennium Bug, refers to the computer programming glitch that was anticipated to cause widespread problems when the year changed from 1999 to 2000. It sparked concerns about system failures in computers and other devices due to date-related issues. Nothing happened and the fears around Y2K turned out to be largely unfounded.",
    image: "y2k.png",
  },
  {
    date: "2000",
    title: "Limewire",
    subtitle: "Peer-to-peer file sharing software",
    content:
      "Limewire, a popular peer-to-peer file sharing application, allows users to share files over the Internet. It becomes widely used for downloading music, movies, and other media files.",
    image: "limewire.png",
  },
  {
    date: "2001",
    title: "Apple iPod",
    subtitle: "The beginning of the era of digital music players",
    content:
      "Apple releases the iPod, a small, portable device that can hold thousands of songs in one's pocket. The iPod revolutionized the way people listen to music and set the stage for the digital music era.",
    image: "ipod.jpg",
  },
  {
    date: "2002",
    title: "Amazon Web Services",
    subtitle: "Amazon ushers in the era of cloud computing",
    content:
      "Amazon launches Amazon Web Services (AWS), which popularizes the concept of cloud computing and the ability to quickly scale web applications. AWS revolutionized the way businesses host and scale their digital services.",
    image: "aws.jpg",
  },
  {
    date: "2003",
    title: "Apple iTunes Store",
    subtitle: "Music that can be purchased and downloaded online",
    content:
      "The Apple iTunes Store is launched, which becomes the most popular platform for purchasing and downloading digital music. It transformed the music industry and how people acquire music.",
    image: "itunes-store.jpg",
  },
  {
    date: "2003",
    title: "MySpace",
    subtitle: "Social networking platform",
    content:
      "MySpace is one of the earliest social networking platforms that allows users to create personalized profiles, connect with friends, and share content. It becomes immensely popular before the rise of Facebook.",
    image: "myspace.png",
  },
  {
    date: "2004",
    title: "Facebook",
    subtitle: "The rise of social media",
    content:
      "Mark Zuckerberg and his college roommates launch Facebook, initially as a social networking platform for Harvard University students. Facebook would go on to become the world's largest social media platform, changing how people connect and share online.",
    image: "facebook.jpg",
  },
  {
    date: "2004",
    title: "Ubuntu",
    subtitle: "A popular open-source Linux distribution",
    content:
      "Ubuntu, a user-friendly Linux distribution, is released, aiming to make Linux more accessible to everyday users. It becomes one of the most popular Linux distributions and is known for its ease of use.",
    image: "ubuntu.png",
  },
  {
    date: "2005",
    title: "Ruby on Rails",
    subtitle: "A popular web framework",
    content:
      "Ruby on Rails, often simply Rails, is released as an open-source web application framework written in Ruby. It provides developers with a structured way to build web applications quickly and efficiently, leading to its widespread adoption in web development.",
    image: "ruby-on-rails.png",
  },
  {
    date: "2005",
    title: "YouTube",
    subtitle: "A platform for sharing videos online",
    content:
      "YouTube is founded, allowing users to upload, share, and view videos on the Internet. It quickly becomes one of the most popular video-sharing platforms and a major source of online entertainment.",
    image: "youtube.jpg",
  },
  {
    date: "2006",
    title: "Twitter",
    subtitle: "The microblogging platform",
    content:
      'Twitter is launched, introducing the concept of microblogging and allowing users to share short messages or "tweets." It becomes a major platform for real-time news and communication.',
    image: "twitter.jpg",
  },
  {
    date: "2007",
    title: "Go Programming Language",
    subtitle: "Efficiency and concurrency",
    content:
      "Go, also known as Golang, is a statically typed, compiled language developed by Google. It excels in efficiency and concurrency support. Go is often used for building scalable and high-performance systems, such as web servers and distributed applications.",
  },
  {
    date: "2007",
    title: "Apple iPhone",
    subtitle: "The smartphone that changed everything",
    content:
      "Apple introduces the iPhone, a revolutionary smartphone that combines a phone, an iPod, and an Internet communication device into one. The iPhone's launch marks the beginning of the smartphone era and changes how people interact with technology.",
    image: "iphone.jpg",
  },
  {
    date: "2008",
    title: "Google Chrome",
    subtitle: "A popular web browser",
    content:
      "Google Chrome is released, quickly becoming one of the most popular web browsers. Its speed, simplicity, and integration with Google services make it a dominant force in the browser market.",
    image: "google-chrome.png",
  },
  {
    date: "2008",
    title: "First Android Phone",
    subtitle: "HTC Dream",
    content:
      "The HTC Dream, also known as the T-Mobile G1, is released as the first commercially available smartphone running the Android operating system. It marks the beginning of Android's dominance in the mobile market.",
    image: "htc-dream.png",
  },
  {
    date: "2009",
    title: "Bitcoin",
    subtitle: "The birth of cryptocurrency",
    content:
      "Bitcoin, the first decentralized digital currency, is created by an unknown person or group of people using the name Satoshi Nakamoto. It introduces the concept of blockchain technology and opens the door to the world of cryptocurrencies.",
  },
  {
    date: "2010",
    title: "Rust Programming Language",
    subtitle: "Safety and performance",
    content:
      "Rust is a systems programming language that prioritizes safety and performance. It's designed to prevent memory-related bugs like null pointer dereferences and buffer overflows. Rust is frequently chosen for projects that require low-level control and high performance.",
  },
  {
    date: "2010",
    title: "First Windows Phone",
    subtitle: "Windows Phone 7",
    content:
      "Windows Phone 7 is launched as Microsoft's first attempt to compete in the smartphone market. It introduces a new user interface and integrates with Microsoft services, but struggles to gain significant market share.",
    image: "windows-phone-7.jpg",
  },
  {
    date: "2010",
    title: "Apple iPad",
    subtitle: "The tablet that popularized a new form factor",
    content:
      "Apple releases the iPad, popularizing the tablet computer. It becomes a versatile device for various applications, from entertainment to productivity.",
    image: "ipad.jpg",
  },
  {
    date: "2010",
    title: "Instagram",
    subtitle: "The rise of photo sharing",
    content:
      "Instagram is launched, focusing on photo and video sharing. It quickly gains popularity and becomes a major social media platform.",
    image: "instagram.png",
  },
  {
    date: "2011",
    title: "D-Wave One",
    subtitle: "Pioneering quantum annealer",
    content:
      "The D-Wave One, introduced in 2011, is considered one of the early quantum computing devices. It was a pioneering quantum annealer designed for solving specific optimization problems using quantum annealing techniques. While it was not a general-purpose quantum computer, it marked a significant milestone in the development of quantum computing technology.",
    image: "d-wave-one.jpg",
  },
  {
    date: "2011",
    title: "Google+",
    subtitle: "Google's social networking platform",
    content:
      "Google+ is introduced as Google's social networking platform, aiming to compete with Facebook. While it had its following, it eventually shut down in 2019.",
    image: "google-plus.png",
  },
  {
    date: "2013",
    title: "Docker",
    subtitle: "Containerization platform",
    content:
      "Docker, an open-source platform, enables developers to automate the deployment of applications inside lightweight, portable containers. It revolutionizes software development by making it easier to build, deploy, and run applications.",
  },
  {
    date: "2013",
    title: "Google Glasses",
    subtitle: "Wearable augmented reality glasses",
    content:
      "Google Glasses, also known as Google Glass, was a wearable computer with an optical head-mounted display. It allowed users to access information hands-free and interact with the digital world through voice commands and gestures. Although it generated excitement, it faced privacy and social acceptance challenges and was discontinued in 2015.",
    image: "google-glasses.jpg",
  },
  {
    date: "2014",
    title: "Facebook's Acquisition of Oculus VR",
    subtitle: "Virtual reality goes mainstream",
    content:
      "Facebook acquires Oculus VR, a company specializing in virtual reality technology. This acquisition marks a significant step toward bringing virtual reality into the mainstream.",
    image: "oculus-vr.jpg",
  },
  {
    date: "2015",
    title: "Ethereum",
    subtitle: "Smart contracts and decentralized applications",
    content:
      "Ethereum, a blockchain platform, is introduced, allowing developers to create and deploy smart contracts and decentralized applications (DApps). It expands the possibilities of blockchain technology beyond digital currency, enabling a wide range of innovative applications.",
  },
  {
    date: "2015",
    title: "Apple Watch",
    subtitle: "Smartwatch",
    content:
      "Apple introduces the Apple Watch, a wearable device that combines fitness tracking, notifications, and apps in a compact form factor. It becomes one of the most popular smartwatches on the market, influencing the wearable technology industry.",
    image: "apple-watch.jpg",
  },
  {
    date: "2015",
    title: "Amazon Echo",
    subtitle: "Voice-activated smart speaker",
    content:
      "Amazon introduces the Echo, a voice-activated smart speaker powered by the virtual assistant Alexa. It popularizes the concept of smart speakers and voice-controlled home automation.",
    image: "amazon-echo.jpg",
  },
  {
    date: "2016",
    title: "Pokémon GO",
    subtitle: "Augmented reality mobile game",
    content:
      "Pokémon GO is released, becoming a cultural phenomenon. It combines augmented reality with location-based gaming, encouraging players to explore their surroundings and catch virtual Pokémon.",
    image: "pokemon-go.jpg",
  },
  {
    date: "2017",
    title: "Tesla Model 3",
    subtitle: "Electric vehicle for the masses",
    content:
      "Tesla introduces the Model 3, an affordable electric car that aims to bring electric vehicles to a wider audience. It garners significant interest and accelerates the adoption of electric cars.",
    image: "tesla-model-3.jpg",
  },
  {
    date: "2019",
    title: "IBM Q System One",
    subtitle: "First quantum computer",
    content:
      "IBM Q System One, introduced in 2019, is considered one of the first modern quantum computers designed for commercial use and accessibility. It marked a significant milestone in the development of quantum computing technology, aiming to make quantum computing more accessible to researchers and businesses.",
    image: "ibm-q-system-1.jpg",
  },
  {
    date: "2019",
    title: "SpaceX Crew Dragon",
    subtitle: "Private spacecraft for human spaceflight",
    content:
      "SpaceX's Crew Dragon spacecraft completes its first uncrewed test flight to the International Space Station, marking a significant milestone in commercial space travel.",
    image: "spacex-dragon.jpg",
  },
  {
    date: "2020",
    title: "Apple Silicon Chip",
    subtitle: "Custom ARM-based processors",
    content:
      "Apple introduces its custom ARM-based processors, known as Apple Silicon, for its Mac lineup. These chips offer improved performance, power efficiency, and integration with macOS, marking a significant shift away from Intel processors.",
    image: "apple-silicon-m1.jpg",
  },
  {
    date: "2020",
    title: "ChatGPT",
    subtitle: "A revolutionary language generation model",
    content:
      "ChatGPT is a state-of-the-art language generation model developed by OpenAI. It uses deep learning techniques to generate human-like text based on the input it receives. ChatGPT finds applications in various fields, including customer support, content creation, and education.",
  },
  {
    date: "2021",
    title: "GitHub Copilot",
    subtitle: "AI-powered code completion tool",
    content:
      "GitHub Copilot is introduced as an AI-powered code completion tool developed by GitHub and OpenAI. It assists developers in writing code by providing suggestions and auto-completions based on context, significantly improving productivity.",
  },
  {
    date: "2021",
    title: "SpaceX Starship",
    subtitle: "A fully reusable spacecraft for Mars colonization",
    content:
      "SpaceX continues development and testing of its Starship spacecraft, which is intended to be a fully reusable spacecraft for missions to Mars and beyond. It represents a significant step toward achieving interplanetary travel.",
    image: "spacex-starship.jpg",
  },
  {
    date: "2023",
    title: "Apple Vision Pro",
    subtitle: "Apple's augmented reality eyewear",
    content:
      "Apple Vision Pro is Apple's foray into augmented reality (AR) eyewear. It is expected to provide users with immersive AR experiences, blending the digital and physical worlds. As of 2023, it's a highly anticipated product, and its features and capabilities are yet to be fully revealed.",
    image: "apple-vision-pro.jpg",
  },
];

export default data;
