export type ImageModalProps = {
  modalOpen: boolean;
  src?: string;
  title?: string;
  onCloseModal: () => void;
};
