import React from "react";
import { Modal } from "react-responsive-modal";
import { ImageModalProps } from "./types";
import "react-responsive-modal/styles.css";
import styles from "./styles.less";

const maxWidth = window.innerWidth * 0.9;
const maxHeight = window.innerHeight * 0.9;

const ImageModal = ({ modalOpen, src, title, onCloseModal }: ImageModalProps) => (
  <Modal center open={modalOpen} onClose={onCloseModal}>
    <div className={styles.imageModal} style={{ maxWidth, maxHeight }}>
      <div className={styles.heading}>{title}</div>
      <img src={require(`Assets/${src}`).default} alt={title} className={styles.image} />
    </div>
  </Modal>
);

export default ImageModal;
