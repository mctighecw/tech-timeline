import React from "react";
import Timeline from "@/components/timeline";
import NavigationBar from "@/components/navigation-bar";
import Controls from "@/components/controls";
import Footer from "@/components/footer";
import useTheme from "@/hooks/use-theme";
import "@/styles/global.css";
import styles from "./styles.less";

const App = () => {
  const { getStyles } = useTheme();
  const applyStyles = (className: string) => getStyles(className, styles);

  return (
    <div className={styles.app}>
      <div className={styles.container}>
        <Controls />
        <NavigationBar />

        <div className={applyStyles(styles.content)}>
          <div className={applyStyles(styles.description)} id="subheading">
            Scroll down to see events in technology history, from the 1950s to the present. The pace of
            technological change over the past 70 years has been impressive!
          </div>

          <div className={styles.divider} />
          <Timeline />
        </div>
      </div>

      <Footer />
    </div>
  );
};

export default App;
