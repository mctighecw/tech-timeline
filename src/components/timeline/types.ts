export type ModalImage = {
  src: string;
  title: string;
};
