import React, { useEffect, useState } from "react";
import { isMobile, isTablet } from "react-device-detect";
import cn from "classnames";
import ImageModal from "@/components/image-modal";
import TimelineItem from "@/components/timeline-item";
import { TimelineItem as TimelineItemType } from "@/components/timeline-item/types";
import useScrollYPosition from "@/hooks/use-scroll-y-position";
import data from "@/constants/data";
import { ModalImage } from "./types";
import styles from "./styles.less";

const Timeline = () => {
  const [items, setItems] = useState<TimelineItemType[]>([]);
  const [modalImage, setModalImage] = useState<ModalImage | null>(null);

  const deviceStyle = isMobile && !isTablet ? styles.mobile : styles.desktop;
  const offset = isMobile && !isTablet ? -200 : 0;
  const { isBottom } = useScrollYPosition(offset);

  const handleClickImage = (src: string, title: string) => {
    setModalImage({ src, title });
  };

  useEffect(() => {
    if (isBottom && items.length < data.length) {
      setItems((prev) => [...prev, data[prev.length]]);
    }
  }, [items, isBottom]);

  return (
    <div className={cn(styles.timeline, deviceStyle)}>
      {items.map((item: any, index: number) => (
        <TimelineItem
          key={item.title}
          data={item}
          previousDate={data[index - 1]?.date}
          onClickImage={handleClickImage}
        />
      ))}

      {!!modalImage && (
        <ImageModal
          modalOpen={!!modalImage}
          src={modalImage.src}
          title={modalImage.title}
          onCloseModal={() => setModalImage(null)}
        />
      )}
    </div>
  );
};

export default Timeline;
