import React, { useContext } from "react";
import { DarkModeToggle } from "react-dark-mode-toggle-2";
import { isMobile } from "react-device-detect";
import { ConfigurationContext } from "@/context/configuration";
import useScrollYPosition from "@/hooks/use-scroll-y-position";
import useTheme from "@/hooks/use-theme";
import styles from "./styles.less";

const Controls = () => {
  const { animation, toggleAnimation } = useContext(ConfigurationContext);
  const { scrollY } = useScrollYPosition();
  const { theme, toggleTheme, getStyles } = useTheme();
  const applyStyles = (className: string) => getStyles(className, styles);

  const isAnimationEnabled = animation === "enabled";
  const isDarkMode = theme === "dark";
  const isScrolled = scrollY > 40;

  const handleScrollUp = () => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  if (isMobile || !isScrolled) {
    return (
      <div className={applyStyles(styles.controlsDefault)}>
        <DarkModeToggle size="3rem" isDarkMode={isDarkMode} onChange={toggleTheme} />
      </div>
    );
  }

  return (
    <div className={applyStyles(styles.controlsScrolled)}>
      <DarkModeToggle size="3rem" isDarkMode={isDarkMode} onChange={toggleTheme} />

      <div className={applyStyles(styles.label)} onClick={toggleAnimation}>
        {isAnimationEnabled ? "Fade" : "No Fade"}
      </div>
      <div className={applyStyles(styles.label)} onClick={handleScrollUp}>
        Scroll Up
      </div>
    </div>
  );
};

export default Controls;
