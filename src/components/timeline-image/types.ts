export type TimelineImageProps = {
  name?: string;
  title?: string;
  onClick: (name: string, title: string) => void;
};
