import React from "react";
import useTheme from "@/hooks/use-theme";
import styles from "./styles.less";
import { TimelineImageProps } from "./types";

const TimelineImage = ({ name, title, onClick }: TimelineImageProps) => {
  const { getStyles } = useTheme();
  const applyStyles = (className: string) => getStyles(className, styles);

  return (
    <div className={styles.timelineImage}>
      {name && title ? (
        <img
          src={require(`Assets/${name}`).default}
          alt={title}
          className={applyStyles(styles.image)}
          onClick={() => onClick(name, title)}
        />
      ) : null}
    </div>
  );
};

export default TimelineImage;
