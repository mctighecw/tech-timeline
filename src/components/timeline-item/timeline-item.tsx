import React from "react";
import cn from "classnames";
import TimelineImage from "@/components/timeline-image";
import { ConfigurationContext } from "@/context/configuration";
import useTheme from "@/hooks/use-theme";
import styles from "./styles.less";
import { TimelineItemProps } from "./types";

const TimelineItem = ({ data, previousDate, onClickImage }: TimelineItemProps) => {
  const { animation } = React.useContext(ConfigurationContext);
  const { getStyles } = useTheme();
  const applyStyles = (className: string) => getStyles(className, styles);

  const isAnimationEnabled = animation === "enabled";
  const contentStyles = isAnimationEnabled ? cn(styles.contentBox, styles.animate) : styles.contentBox;

  const dateValue = data.date === previousDate ? null : data.date;

  return (
    <div className={styles.timelineItem}>
      <div className={styles.date}>{dateValue}</div>
      <div className={applyStyles(styles.line)} />
      <div className={styles.dot} />

      <div className={contentStyles}>
        <div className={applyStyles(styles.title)}>{data.title}</div>
        <div className={applyStyles(styles.subtitle)}>{data.subtitle}</div>
        <div className={applyStyles(styles.content)}>{data.content}</div>

        <TimelineImage name={data.image} title={data.title} onClick={onClickImage} />

        {data?.link ? (
          <a href={data.link} target="_blank" rel="noopener noreferrer" className={styles.link}>
            More Info
          </a>
        ) : null}
      </div>
    </div>
  );
};

export default TimelineItem;
