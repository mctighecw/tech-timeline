export type TimelineItem = {
  date: string;
  title: string;
  subtitle: string;
  content: string;
  image?: string;
  link?: string;
};

export type TimelineItemProps = {
  data: TimelineItem;
  previousDate: string;
  onClickImage: (src: string, title: string) => void;
};
