import React from "react";
import useTheme from "@/hooks/use-theme";
import styles from "./styles.less";

const NavigationBar = () => {
  const { getStyles } = useTheme();
  const applyStyles = (className: string) => getStyles(className, styles);

  return (
    <div className={applyStyles(styles.navigationBar)}>
      <div />
      <div className={applyStyles(styles.heading)}>Tech Timeline</div>
    </div>
  );
};

export default NavigationBar;
