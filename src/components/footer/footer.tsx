import React from "react";
import { isMobile } from "react-device-detect";
import useTheme from "@/hooks/use-theme";
import styles from "./styles.less";

const Footer = () => {
  const { getStyles } = useTheme();
  const applyStyles = (className: string) => getStyles(className, styles);
  const footerStyles = isMobile ? styles.none : applyStyles(styles.footer);

  return (
    <div className={footerStyles}>
      <div className={applyStyles(styles.text)}>© 2023 Christian McTighe. Coded by Hand.</div>
    </div>
  );
};

export default Footer;
