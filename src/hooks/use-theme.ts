import React from "react";
import { ThemeContext } from "@/context/theme";
import cn from "classnames";

const useTheme = () => {
  const { theme, toggleTheme } = React.useContext(ThemeContext);

  // get light/dark theme styles
  const getStyles = (className?: string, styles?: any) => {
    if (className && styles) {
      const themeStyle = theme === "light" ? styles.light : styles.dark;
      return cn(className, themeStyle);
    }
    return;
  };

  return { theme, toggleTheme, getStyles };
};

export default useTheme;
