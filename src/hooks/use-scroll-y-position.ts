import { useState, useEffect } from "react";

const useScrollYPosition = (offset: number = 0) => {
  const [scrollY, setScrollY] = useState<number>(offset);

  const recordScroll = () => {
    setScrollY(window.scrollY);
  };

  useEffect(() => {
    window.addEventListener("scroll", recordScroll, true);

    return () => {
      window.removeEventListener("scroll", recordScroll);
    };
  }, []);

  const isBottom = window.innerHeight + Math.round(scrollY) >= document.body.offsetHeight;

  return { scrollY, isBottom };
};

export default useScrollYPosition;
