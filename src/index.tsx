import React from "react";
import * as ReactDOMClient from "react-dom/client";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import App from "@/components/app";
import { ConfigurationProvider } from "@/context/configuration";
import { ThemeProvider } from "@/context/theme";

const container = document.getElementById("root");
const root = ReactDOMClient.createRoot(container);

root.render(
  <ConfigurationProvider>
    <ThemeProvider>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<App />} />
          <Route path="*" element={<Navigate to="/" replace />} />
        </Routes>
      </BrowserRouter>
    </ThemeProvider>
  </ConfigurationProvider>
);
