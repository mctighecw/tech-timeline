export type ThemeType = "light" | "dark";

const getTheme = () => {
  return localStorage.getItem("theme") as ThemeType;
};

export const setTheme = (theme: ThemeType) => {
  localStorage.setItem("theme", theme);
};

export const checkAndSetTheme = () => {
  let theme: ThemeType | null = getTheme();

  if (!theme) {
    theme = matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
    setTheme(theme);
  }

  return theme;
};
