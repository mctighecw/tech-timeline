export type AnimationType = "enabled" | "disabled";

const getAnimation = () => {
  return localStorage.getItem("animation") as AnimationType;
};

export const setAnimation = (animation: AnimationType) => {
  localStorage.setItem("animation", animation);
};

export const checkAndSetAnimation = () => {
  let animation: AnimationType | null = getAnimation();

  if (!animation) {
    setAnimation("enabled");
  }

  return animation;
};
