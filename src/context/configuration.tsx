import React, { useState, createContext, ReactNode } from "react";
import {
  checkAndSetAnimation,
  setAnimation as setAnimationInStorage,
  AnimationType,
} from "@/utils/animation";

type ConfigurationProviderType = {
  children: ReactNode;
};

const defaultAnimation = checkAndSetAnimation();

const ConfigurationContext = createContext({ animation: defaultAnimation, toggleAnimation: () => {} });

const ConfigurationProvider = ({ children }: ConfigurationProviderType) => {
  const [animation, setAnimation] = useState<AnimationType>(defaultAnimation);

  const toggleAnimation = () => {
    const value = animation === "enabled" ? "disabled" : "enabled";

    setAnimationInStorage(value);
    setAnimation(value);
  };

  return (
    <ConfigurationContext.Provider value={{ animation, toggleAnimation }}>
      {children}
    </ConfigurationContext.Provider>
  );
};

export { ConfigurationContext, ConfigurationProvider };
