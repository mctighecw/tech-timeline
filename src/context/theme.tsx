import React, { useState, createContext, ReactNode } from "react";
import { setTheme as setThemeInStorage, checkAndSetTheme, ThemeType } from "@/utils/theme";

type ThemeProviderType = {
  children: ReactNode;
};

const defaultTheme = checkAndSetTheme();

const ThemeContext = createContext({ theme: defaultTheme, toggleTheme: () => {} });

const ThemeProvider = ({ children }: ThemeProviderType) => {
  const [theme, setTheme] = useState<ThemeType>(defaultTheme);

  const toggleTheme = () => {
    const value = theme === "light" ? "dark" : "light";

    setThemeInStorage(value);
    setTheme(value);
  };

  return <ThemeContext.Provider value={{ theme, toggleTheme }}>{children}</ThemeContext.Provider>;
};

export { ThemeContext, ThemeProvider };
