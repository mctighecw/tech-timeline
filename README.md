# README

A scrollable tech timeline app. Most of the texts were composed with the help of ChatGPT.

## App Information

App Name: tech-timeline

Created: September-October 2023

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/tech-timeline)

Production: [Link](https://tech-timeline.mctighecw.site)

## Tech Stack

- TypeScript
- React
- React Router
- Webpack
- Less
- CSS Modules
- Prettier
- ESLint

## Packages Audit

It is necessary to run a JavaScript package audit periodically, to see if there are any critical or high vulnerabilites.

Refer to the [Yarn docs](https://classic.yarnpkg.com/lang/en/docs/cli/audit/).

```sh
$ yarn audit --level high
```

If so, add the correct package versions to "resolutions" in the `package.json` file.

## To Run

_Development_

```
$ yarn install
$ yarn start
```

_Production_

Using `docker run`

```sh
$ docker build \
  -f Dockerfile.prod \
  -t tech-timeline . \
  && docker run \
  --rm -it \
  --name tech-timeline \
  -p 80:80 \
  tech-timeline
```

Last updated: 2025-01-30
