const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const TSConfigPathsPlugin = require("tsconfig-paths-webpack-plugin");

module.exports = {
  entry: path.resolve(__dirname, "src/index.tsx"),
  resolve: {
    extensions: [".ts", ".tsx", ".js"],
    alias: {
      Assets: path.resolve(__dirname, "src/assets/"),
      LessConstants: path.resolve(__dirname, "src/styles/constants.less"),
    },
    plugins: [new TSConfigPathsPlugin()],
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: "babel-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "src/index.html"),
      favicon: path.resolve(__dirname, "src/assets/favicon.png"),
    }),
  ],
};
