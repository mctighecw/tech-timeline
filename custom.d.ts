declare module "react-dom/client";

declare module "*.css";
declare module "*.less";

declare module "*.jpg";
declare module "*.jpeg";
declare module "*.png";

declare module "*.svg" {
  const content: any;
  export default content;
}
